/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */

export const preloadInsaneHandlebarsTemplates = async function() {

    // Define template paths to load
    const insaneTempPaths =
        [
      // Actor Sheet Partials
      //"modules/insane-foundry-module/templates/parts/actor-traits.html",
      "modules/insane-foundry-module/templates/parts/actor-inventory.html"
      //"modules/insane-foundry-module/templates/parts/actor-features.html",
      //"modules/insane-foundry-module/templates/parts/actor-spellbook.html"
        ];

    // Load the template parts
    return loadTemplates(insaneTempPaths);
  };
