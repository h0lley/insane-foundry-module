
import { DND5E } from "../../../systems/dnd5e/module/config.js"
import ActorSheet5e from "../../../systems/dnd5e/module/actor/sheets/base.js"
import ActorSheet5eCharacter from "../../../systems/dnd5e/module/actor/sheets/character.js"

import { preloadInsaneHandlebarsTemplates } from "./insane-templates.js"

Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

export class INSANEsheet extends ActorSheet5eCharacter
    {
	get template ()
        {
        //return "../templates/character-sheet.html"
        return "modules/insane-foundry-module/templates/character-sheet.html"
        }
    static get defaultOptions()
        {
        return mergeObject(super.defaultOptions,
                {
                classes: ["insane", "dnd5e", "sheet", "actor", "character"],
                })
        }
    // save all simultaneously open editor field when one field is saved
	async _onEditorSave(target, element, content) {
        return this.submit();
      }
      activateListeners(html)
        {
		super.activateListeners(html);
        html.find('.item:not(.inventory-header) input').change(event =>
                {
            let value = event.target.value
            let actor = this.actor
            let itemId = $(event.target).parents('.item')[0].dataset.itemId
            let path = event.target.dataset.path
            let data = {}
            data[path] = Number(event.target.value)
            console.log(data)
            actor.getOwnedItem(itemId).update(data)
                });

        html.find('.attribute.initiative h4.attribute-name').click((event) =>
            {
            // Roll initiative if there is an encounter (gm starts encounter)
            console.log(this.actor)
            enterCombatAndRollInit(this.actor.data.token.name)
            })
        }
    }

async function enterCombatAndRollInit(act)
    {
    // get field token whos from the active sheet (actor)
    let token = canvas.tokens.ownedTokens.find(c => c.name == act)
    // gm start encounter
    if(!game.combat && game.user.isGM)
        {
        let scene = game.scenes.viewed
        if ( !scene ) return
        let cbt = await game.combats.object.create({scene: scene._id})
        await cbt.activate()
        }

    if(!game.combat && !game.user.isGM)
        {
        ui.notifications.notify("There is no active encounter")
        }
    else if(game.combat)
        {
        if(!token.inCombat)
            await token.toggleCombat()
        let cId = await game.combat.getCombatantByToken(token.data._id)
        game.combat.rollInitiative(cId._id)
        }
    }

function getOwnedToken()
    {
      var controlled = game.user.isGM && this.actor.isNPC ? canvas.tokens.ownedTokens[0] : canvas.tokens.controlled;
      return controlled;
    }
// Register Tidy5e Sheet and make default character sheet
Actors.registerSheet("dnd5e", INSANEsheet,
    {
	types: ["character"],
	makeDefault: true
    });

Hooks.once("init", () =>
    {
    preloadInsaneHandlebarsTemplates()
    })

Hooks.on("ready", function()
    {
    if (window.BetterRolls)
        {
        window.BetterRolls.hooks.addActorSheet("INSANEsheet");
        }
    });
