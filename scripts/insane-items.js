import ItemSheet5e from "../../../systems/dnd5e/module/item/sheet.js";

export class INSANEItemSheet extends ItemSheet5e
    {
    static get defaultOptions()
        {
        return mergeObject(super.defaultOptions,
                {
                classes: ["insane", "dnd5e", "sheet", "item"]
                })
        }
    }

Items.registerSheet("dnd5e", INSANEItemSheet, {makeDefault: true});

Hooks.once("ready", () =>
    {
    if(window.BetterRolls)
        {
        window.BetterRolls.hooks.addItemSheet("INSANEItemSheet")
        }
    })
